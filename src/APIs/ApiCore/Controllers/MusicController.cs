﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Core.Resources;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Core.Models;
using Core.Resources.Save;
using Core.Resources.Validators;
using Core.Services;

namespace ApiCore.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class MusicController : ControllerBase
    {
        private readonly IMusicService _service;
        private readonly IMapper _mapper;

        public MusicController(IMusicService service, IMapper mapper)
        {
            _service = service;
            _mapper = mapper;
        }

        [HttpGet("")]
        public async Task<ActionResult<IEnumerable<MusicResource>>> GetAllMusics() =>
            Ok(_mapper
                .Map<
                    IEnumerable<Music>,
                    IEnumerable<MusicResource>
                >(await _service.GetAllWithArtist()));

        [HttpGet("{id}")]
        public async Task<ActionResult<MusicResource>> GetMusicById(int id) =>
            Ok(_mapper
                .Map<
                    Music,
                    MusicResource>(
                    await _service.GetMusicById(id)
                ));

        [HttpPost]
        public async Task<ActionResult<MusicResource>> CreateMusic([FromBody] SaveMusicResource resource)
        {
            var validationResult = await new SaveMusicResourceValidator().ValidateAsync(resource);
            if (!validationResult.IsValid)
            {
                return BadRequest(validationResult.Errors);
            }

            return Ok(_mapper
                .Map<
                    Music,
                    MusicResource>(
                    await _service
                        .GetMusicById(
                            (await _service
                                .CreateMusic(
                                    _mapper
                                        .Map<SaveMusicResource, Music>(
                                            resource
                                        )
                                )
                            ).Id
                        )
                )
            );
        }
    }
}