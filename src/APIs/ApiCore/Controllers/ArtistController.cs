﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Core.Resources;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Core.Models;
using Core.Services;

namespace ApiCore.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ArtistController : ControllerBase
    {
        private readonly IArtistService _service;
        private readonly IMapper _mapper;

        public ArtistController(IArtistService service, IMapper mapper)
        {
            _service = service;
            _mapper = mapper;
        }

        [HttpGet]
        public async Task<ActionResult<IEnumerable<ArtistResource>>> GetAllArtist() =>
            Ok(_mapper
                .Map<
                    IEnumerable<Artist>,
                    IEnumerable<ArtistResource>>(
                    await _service.GetAllArtists()
                )
            );

        [HttpGet("{id}")]
        public async Task<ActionResult<IEnumerable<ArtistResource>>> GetArtistById(int id) =>
            Ok(_mapper
                .Map<
                    Artist,
                    ArtistResource>(
                        await _service.GetArtistById(id)
                )
            );
    }
}