﻿using AutoMapper;
using Core.Models;
using Core.Resources;
using Core.Resources.Save;

namespace ApiCore.Mapping
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            CreateMap<Music, MusicResource>();
            CreateMap<Artist, ArtistResource>();
            
            CreateMap<MusicResource, Music>();
            CreateMap<ArtistResource, Artist>();
            
            CreateMap<SaveMusicResource, Music>();
            CreateMap<SaveArtistResource, Artist>();
        }
    }
}