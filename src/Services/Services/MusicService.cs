﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Core;
using Core.Models;
using Core.Services;

namespace Services
{
    public class MusicService : IMusicService
    {
        private readonly IUnitOfWork _unit;

        public MusicService(IUnitOfWork unit)
        {
            _unit = unit;
        }

        public async Task<IEnumerable<Music>> GetAllWithArtist() =>
            await _unit.Musics.GetAllWithArtistAsync();

        public async Task<Music> GetMusicById(int id) =>
            await _unit.Musics.GetWithArtistByIdAsync(id);

        public async Task<IEnumerable<Music>> GetMusicsByArtistId(int artistId) =>
            await _unit.Musics.GetAllWithArtistByArtistIdAsync(artistId);

        public async Task<Music> CreateMusic(Music newMusic)
        {
            await _unit.Musics.AddAsync(newMusic);
            await _unit.CommitAsync();
            return newMusic;
        }

        public async Task UpdateMusic(Music musicToBeUpdated, Music music)
        {
            musicToBeUpdated.Name = music.Name;
            music.ArtistId = music.ArtistId;
            await _unit.CommitAsync();
        }

        public async Task DeleteMusic(Music music)
        {
            _unit.Musics.Remove(music);
            await _unit.CommitAsync();
        }
    }
}