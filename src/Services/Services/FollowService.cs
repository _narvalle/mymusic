﻿using System.Threading.Tasks;
using Core;
using Core.Models;
using Core.Services;

namespace Services
{
    public class FollowService : IFollowService
    {
        private readonly IUnitOfWork _unit;

        public FollowService(IUnitOfWork unit)
        {
            _unit = unit;
        }
        
        public async Task<bool> CreateFollow(Follow follow)
        {
            await _unit.Follows.AddAsync(follow);
            return await _unit.CommitAsync() > 0;
        }

        public async Task<int> CountFollowByArtist(int artistId) =>
            await _unit.Follows.CountFollowByArtist(artistId);

        public async Task<Artist> GetArtistMoreFollow() =>
            await _unit.Follows.GetArtistMoreFollow();
    }
}