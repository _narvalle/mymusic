﻿using System.Threading.Tasks;
using Core;
using Core.Models;
using Core.Services;

namespace Services
{
    public class LikeService : ILikeService
    {
        private readonly IUnitOfWork _unit;

        public LikeService(IUnitOfWork unit)
        {
            _unit = unit;
        }
        
        public async Task<bool> CreateLike(Like like)
        {
            await _unit.Likes.AddAsync(like);
            return await _unit.CommitAsync() > 0;
        }

        public async Task<int> CountLikeByMusic(int musicId) =>
            await _unit.Likes.CountLikeByMusic(musicId);

        public async Task<Music> GetMusicMoreLike() =>
            await _unit.Likes.GetMusicMoreLike();
    }
}