﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Core;
using Core.Models;
using Core.Services;

namespace Services
{
    public class ArtistService : IArtistService, Core.Services.APIs.IArtistService
    {
        private readonly IUnitOfWork _unit;
        private readonly IUnitOfWorkAPIs _unitAPIs;

        public ArtistService(IUnitOfWork unit)
        {
            _unit = unit;
        }

        public ArtistService(IUnitOfWorkAPIs unit)
        {
            _unitAPIs = unit;
        }

        public async Task<IEnumerable<Artist>> GetAllArtists() =>
            await _unit.Artists.GetAllWithMusicsAsync();

        public async Task<Artist> GetArtistById(int id) =>
            await _unit.Artists.GetWithMusicsByIdAsync(id);

        public async Task<Artist> CreateArtist(Artist newArtist)
        {
            await _unit.Artists.AddAsync(newArtist);
            await _unit.CommitAsync();
            return newArtist;
        }

        public async Task UpdateArtist(Artist artistToBeUpdated, Artist artist)
        {
            artistToBeUpdated.Name = artist.Name;
            await _unit.CommitAsync();
        }

        public async Task DeleteArtist(Artist artist)
        {
            _unit.Artists.Remove(artist);
            await _unit.CommitAsync();
        }

        public async Task<IEnumerable<Artist>> HttpGetAllAtist() =>
            await _unitAPIs.Artists.HttpGetAllAtist();
    }
}