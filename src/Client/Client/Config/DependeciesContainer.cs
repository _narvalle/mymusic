﻿using System;
using Microsoft.Extensions.Configuration;
using Core;
using Core.Models;
using Core.Repositories.APIs;
using Core.Services.APIs;
using Data;
using Data.Repositories.APIs;
using Microsoft.Extensions.DependencyInjection;
using Services.APIs;

namespace Client.Config
{
    public static class DependeciesContainer
    {
        public static void AddMyDependencies(
            this IServiceCollection serviceCollection,
            IConfiguration configuration
        )
        {
            serviceCollection.AddHttpClient<
                IUnitOfWorkAPIs,
                UnitOfWorkAPIs>()
                .SetHandlerLifetime(
                TimeSpan.FromMinutes(5)
            );
        }
    }
}