﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Core.Models;

namespace Core.Services.APIs
{
    public interface IArtistService
    {
        Task<IEnumerable<Artist>> HttpGetAllAtist();
    }
}