﻿using System.Threading.Tasks;
using Core.Models;

namespace Core.Services
{
    public interface ILikeService
    {
        Task<bool> CreateLike(Like like);
        Task<int> CountLikeByMusic(int musicId);
        Task<Music> GetMusicMoreLike();
    }
}