﻿using System.Threading.Tasks;
using Core.Models;

namespace Core.Services
{
    public interface IFollowService
    {
        Task<bool> CreateFollow(Follow follow);
        Task<int> CountFollowByArtist(int artistId);
        Task<Artist> GetArtistMoreFollow();
    }
}