﻿using System;
using Core.Repositories.APIs;

namespace Core
{
    public interface IUnitOfWorkAPIs : IDisposable
    {
        IArtistRepository Artists { get; }
    }
}