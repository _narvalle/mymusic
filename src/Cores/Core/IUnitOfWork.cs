﻿using System;
using System.Threading.Tasks;
using Core.Repositories;

namespace Core
{
    public interface IUnitOfWork : IDisposable
    {
        IMusicRepository Musics { get; }
        IArtistRepository Artists { get; }
        ILikeRepository Likes { get; }
        IFollowRepository Follows { get; }
        Task<int> CommitAsync();
    }
}