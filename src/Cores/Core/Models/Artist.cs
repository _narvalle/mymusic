﻿using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace Core.Models
{
    public class Artist
    {
        public Artist()
        {
            Musics = new Collection<Music>();
            Follows= new List<Follow>();
        }
        public int Id { get; set; }
        public string Name { get; set; }
        public ICollection<Music> Musics { get; set; }
        public ICollection<Follow> Follows { get; set; }
    }
}