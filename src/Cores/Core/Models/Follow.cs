﻿namespace Core.Models
{
    public class Follow
    {
        public int Id { get; set; }
        public int ArtistId { get; set; }
        public int UserId { get; set; }
        public Artist Artist { get; set; }
        public User User { get; set; }
    }
}