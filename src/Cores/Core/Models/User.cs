﻿using System.Collections.Generic;

namespace Core.Models
{
    public class User
    {
        public User()
        {
            Likes = new List<Like>();
            Follows = new List<Follow>();
        }
        public int Id { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public ICollection<Like> Likes { get; set; }
        public ICollection<Follow> Follows { get; set; }
    }
}