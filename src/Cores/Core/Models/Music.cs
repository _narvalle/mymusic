﻿using System.Collections.Generic;

namespace Core.Models
{
    public class Music
    {
        public Music()
        {
            Likes = new List<Like>();
        }
        public int Id { get; set; }
        public string Name { get; set; }
        public int ArtistId { get; set; }
        public Artist Artist { get; set; }
        public ICollection<Like> Likes { get; set; }
    }
}