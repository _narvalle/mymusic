﻿namespace Core.Models
{
    public class Like
    {
        public int Id { get; set; }
        public int MusicId { get; set; }
        public int UserId { get; set; }
        public Music Music { get; set; }
        public User User { get; set; }
    }
}