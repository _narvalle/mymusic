﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace Core.Repositories.APIs
{
    public interface IRepository<TEntity> where TEntity : class
    {
        ValueTask<TEntity> HttpGet(int id);
        Task<IEnumerable<TEntity>> HttpGet();
        Task<IEnumerable<TEntity>> HttpGet(string text);
        Task HttpPost(object data);
    }
}