﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Core.Models;

namespace Core.Repositories.APIs
{
    public interface IArtistRepository : IRepository<Artist>
    {
        Task<IEnumerable<Artist>> HttpGetAllAtist();
    }
}