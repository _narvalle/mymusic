﻿using System.Threading.Tasks;
using Core.Models;

namespace Core.Repositories
{
    public interface IFollowRepository : IRepository<Follow>
    {
        Task<int> CountFollowByArtist(int artistId);
        Task<Artist> GetArtistMoreFollow();
    }
}