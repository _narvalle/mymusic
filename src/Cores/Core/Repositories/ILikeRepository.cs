﻿using System.Threading.Tasks;
using Core.Models;

namespace Core.Repositories
{
    public interface ILikeRepository : IRepository<Like>
    {
        Task<int> CountLikeByMusic(int musicId);
        Task<Music> GetMusicMoreLike();
    }
}