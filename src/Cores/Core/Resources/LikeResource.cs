﻿using Core.Models;

namespace Core.Resources
{
    public class LikeResource
    {
        public int Id { get; set; }
        public Music Music { get; set; }
    }
}