﻿using Core.Resources.Save;
using FluentValidation;

namespace Core.Resources.Validators
{
    public class SaveLikeResourceValidator : AbstractValidator<SaveLikeResource>
    {
        public SaveLikeResourceValidator()
        {
            RuleFor(x => x.MusicId)
                .NotEmpty()
                .When(x => x.MusicId <= 0)
                .WithMessage("The entered MusicId value is not allowed.");
            RuleFor(x => x.UserId)
                .NotEmpty()
                .When(x => x.UserId <= 0)
                .WithMessage("The entered UserId value is not allowed.");
        }
    }
}