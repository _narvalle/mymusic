﻿using Core.Resources.Save;
using FluentValidation;

namespace Core.Resources.Validators
{
    public class SaveFollowResourceValidator : AbstractValidator<SaveFollowResource>
    {
        public SaveFollowResourceValidator()
        {
            RuleFor(x => x.ArtistId)
                .NotEmpty()
                .When(x => x.ArtistId <= 0)
                .WithMessage("The entered MusicId value is not allowed.");
            RuleFor(x => x.UserId)
                .NotEmpty()
                .When(x => x.UserId <= 0)
                .WithMessage("The entered UserId value is not allowed.");
        }
    }
}