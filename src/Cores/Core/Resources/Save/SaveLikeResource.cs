﻿namespace Core.Resources.Save
{
    public class SaveLikeResource
    {
        public int MusicId { get; set; }
        public int UserId { get; set; }
    }
}