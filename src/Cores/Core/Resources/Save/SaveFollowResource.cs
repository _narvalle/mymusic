﻿namespace Core.Resources.Save
{
    public class SaveFollowResource
    {
        public int ArtistId { get; set; }
        public int UserId { get; set; }
    }
}