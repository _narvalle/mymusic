﻿using Core.Models;

namespace Core.Resources
{
    public class FollowResource
    {
        public int Id { get; set; }
        public Artist Artist { get; set; }
    }
}