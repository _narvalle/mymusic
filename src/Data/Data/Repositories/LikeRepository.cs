﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Core.Models;
using Core.Repositories;
using Microsoft.EntityFrameworkCore;

namespace Data.Repositories
{
    public class LikeRepository : Repository<Like>, ILikeRepository
    {
        public LikeRepository(DbContext context) : base(context)
        {
        }

        public async Task<int> CountLikeByMusic(int musicId) =>
            await MyContext.Likes
                .CountAsync(x => x.MusicId == musicId);

        public async Task<Music> GetMusicMoreLike() =>
            await MyContext.Musics.SingleOrDefaultAsync(x =>
                x.Id == MyContext.Likes
                    .GroupBy(y => y.MusicId)
                    .OrderByDescending(y => y.Count())
                    .Select(y => y.Key)
                    .First()
            );

        private MyApplicationDbContext MyContext => _context as MyApplicationDbContext;
    }
}