﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Core.Models;
using Core.Repositories;
using System.Linq;

namespace Data.Repositories
{
    public class MusicRepository : Repository<Music>, IMusicRepository
    {
        public MusicRepository(MyApplicationDbContext context) : base(context)
        {
        }

        public async Task<IEnumerable<Music>> GetAllWithArtistAsync() =>
            await MyContext.Musics
                .Include(x => x.Artist)
                .ToListAsync();


        public async Task<Music> GetWithArtistByIdAsync(int id) =>
            await MyContext.Musics
                .Include(x => x.Artist)
                .SingleOrDefaultAsync(x => x.Id == id);

        public async Task<IEnumerable<Music>> GetAllWithArtistByArtistIdAsync(int artistId) =>
            await MyContext.Musics
                .Include(x => x.Artist)
                .Where(x => x.ArtistId == artistId)
                .ToListAsync();

        private MyApplicationDbContext MyContext
        {
            get => _context as MyApplicationDbContext;
        }
    }
}