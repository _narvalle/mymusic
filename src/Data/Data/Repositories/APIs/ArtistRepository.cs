﻿using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;
using Core.Models;
using Core.Repositories.APIs;
using Newtonsoft.Json;

namespace Data.Repositories.APIs
{
    public class ArtistRepository : Repository<Artist>, IArtistRepository
    {
        private const string ControllerName = "Artist";
        public async Task<IEnumerable<Artist>> HttpGetAllAtist()
        {
            var response = await Client.GetAsync($"{BaseUrl}{ControllerName}");
            if (!response.IsSuccessStatusCode)
            {
                return null;
            }
            return JsonConvert
                .DeserializeObject<IEnumerable<Artist>>(
                    await response.Content.ReadAsStringAsync()
                );
        }

        public ArtistRepository(HttpClient client) : base(client)
        {
        }

        private HttpClient Client => _client;
        private string BaseUrl => _baseUrl;
    }
}