﻿using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Core.Repositories.APIs;
using Newtonsoft.Json;


namespace Data.Repositories.APIs
{
    public class Repository<TEntity> : IRepository<TEntity> where TEntity : class
    {
        protected readonly HttpClient _client;
        protected readonly string _baseUrl = "http://127.0.0.1:5000/api/";

        public Repository(HttpClient client)
        {
            _client = client;
        }

        public async ValueTask<TEntity> HttpGet(int id)
        {
            var response = await _client.GetAsync($"{_baseUrl}{id}");
            if (!response.IsSuccessStatusCode)
            {
                return null;
            }

            return JsonConvert
                .DeserializeObject<TEntity>(
                    await response.Content.ReadAsStringAsync()
                );
        }

        public async Task<IEnumerable<TEntity>> HttpGet()
        {
            var response = await _client.GetAsync(_baseUrl);
            if (!response.IsSuccessStatusCode)
            {
                return null;
            }

            return JsonConvert
                .DeserializeObject<IEnumerable<TEntity>>(
                    await response.Content.ReadAsStringAsync()
                );
        }

        public async Task<IEnumerable<TEntity>> HttpGet(string text)
        {
            var response = await _client.GetAsync($"{_baseUrl}{text}");
            if (!response.IsSuccessStatusCode)
            {
                return null;
            }

            return JsonConvert
                .DeserializeObject<IEnumerable<TEntity>>(
                    await response.Content.ReadAsStringAsync()
                );
        }

        public async Task HttpPost(object data)
        {
            var response = await _client.PostAsync(
                _baseUrl,
                new StringContent(
                    JsonConvert.SerializeObject(data),
                    Encoding.Default,
                    "application/json"
                )
            );
            if (!response.IsSuccessStatusCode)
            {
                return;
            }
            
        }
    }
}