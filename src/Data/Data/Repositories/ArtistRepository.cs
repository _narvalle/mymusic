﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Core.Models;
using Core.Repositories;

namespace Data.Repositories
{
    public class ArtistRepository : Repository<Artist>, IArtistRepository
    {
        public ArtistRepository(DbContext context) : base(context)
        {
        }

        public async Task<IEnumerable<Artist>> GetAllWithMusicsAsync() =>
            await MyContext.Artists
                .Include(x => x.Musics)
                .ToListAsync();

        public async Task<Artist> GetWithMusicsByIdAsync(int id) =>
            await MyContext.Artists
                .Include(x => x.Musics)
                .SingleOrDefaultAsync(x => x.Id == id);

        private MyApplicationDbContext MyContext
        {
            get => _context as MyApplicationDbContext;
        }
    }
}