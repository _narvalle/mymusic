﻿using System.Linq;
using System.Threading.Tasks;
using Core.Models;
using Core.Repositories;
using Microsoft.EntityFrameworkCore;

namespace Data.Repositories
{
    public class FollowRepository : Repository<Follow>, IFollowRepository
    {
        public FollowRepository(DbContext context) : base(context)
        {
        }

        public async Task<int> CountFollowByArtist(int artistId) =>
            await MyContext.Follows
                .CountAsync(x => x.ArtistId == artistId);

        public async Task<Artist> GetArtistMoreFollow() =>
            await MyContext.Artists
                .SingleOrDefaultAsync(x =>
                    x.Id == 
                    MyContext.Follows
                        .GroupBy(y => y.ArtistId)
                        .OrderByDescending(y => y.Count())
                        .Select(y => y.Key)
                        .First()
                );

        private MyApplicationDbContext MyContext => _context as MyApplicationDbContext;
    }
}