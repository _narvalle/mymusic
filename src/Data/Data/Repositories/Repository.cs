﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Core.Repositories;

namespace Data.Repositories
{
    public class Repository<TEntity> : IRepository<TEntity> where TEntity : class
    {
        protected readonly DbContext _context;

        public Repository(DbContext context)
        {
            _context = context;
        }
        
        public async ValueTask<TEntity> GetByIdAsync(int id) =>
            await _context.Set<TEntity>().FindAsync(id);

        public async Task<IEnumerable<TEntity>> GetAllAsync() =>
            await _context.Set<TEntity>().ToListAsync();

        public IEnumerable<TEntity> Find(Expression<Func<TEntity, bool>> expression) =>
            _context.Set<TEntity>().Where(expression);

        public async Task<TEntity> SingleOrDefaultAsync(Expression<Func<TEntity, bool>> expression) =>
            await _context.Set<TEntity>().SingleOrDefaultAsync(expression);

        public async Task AddAsync(TEntity entity) =>
            await _context.Set<TEntity>().AddAsync(entity);

        public async Task AddRangeAsync(IEnumerable<TEntity> entities) =>
            await _context.Set<TEntity>().AddRangeAsync(entities);

        public void Remove(TEntity entity) =>
            _context.Set<TEntity>().Remove(entity);

        public void RemoveRange(IEnumerable<TEntity> entities) =>
            _context.Set<TEntity>().RemoveRange(entities);
    }
}