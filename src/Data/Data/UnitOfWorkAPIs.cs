﻿using System.Net.Http;
using System.Threading.Tasks;
using Core;
using Core.Repositories.APIs;
using Data.Repositories.APIs;

namespace Data
{
    public class UnitOfWorkAPIs : IUnitOfWorkAPIs
    {
        private readonly HttpClient _client;
        private IArtistRepository _artistRepository;

        public UnitOfWorkAPIs(HttpClient client)
        {
            _client = client;
        }

        public IArtistRepository Artists => _artistRepository ??= new ArtistRepository(_client);

        public void Dispose()
        {
            _client.Dispose();
        }
    }
}