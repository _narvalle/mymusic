﻿using System.Threading.Tasks;
using Data.Repositories;
using Core;
using Core.Repositories;

namespace Data
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly MyApplicationDbContext _context;
        private MusicRepository _musicRepository;
        private ArtistRepository _artistRepository;
        private LikeRepository _likeRepository;
        private FollowRepository _followRepository;

        public UnitOfWork( MyApplicationDbContext context)
        {
            _context = context;
        }

        public IMusicRepository Musics => _musicRepository ??= new MusicRepository(_context);
        public IArtistRepository Artists => _artistRepository ??= new ArtistRepository(_context);
        public ILikeRepository Likes => _likeRepository ??= new LikeRepository(_context);
        public IFollowRepository Follows => _followRepository ??= new FollowRepository(_context);

        public async Task<int> CommitAsync() =>
            await _context.SaveChangesAsync();

        public void Dispose()
        {
            _context.Dispose();
        }
    }
}