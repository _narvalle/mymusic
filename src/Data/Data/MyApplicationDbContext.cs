﻿using Data.Configurations;
using Microsoft.EntityFrameworkCore;
using Core.Models;

namespace Data
{
    public class MyApplicationDbContext : DbContext
    {
        public DbSet<Music> Musics { get; set; }
        public DbSet<Artist> Artists { get; set; }
        public DbSet<Like> Likes { get; set; }
        public DbSet<Follow> Follows { get; set; }
        
        public MyApplicationDbContext(
            DbContextOptions<MyApplicationDbContext> options
            ) : base(options){}

        protected override void OnModelCreating(ModelBuilder builder)
        {
            builder.ApplyConfiguration(new MusicConfiguration());
            builder.ApplyConfiguration(new ArtistConfiguration());
            builder.ApplyConfiguration(new LikeConfiguration());
            builder.ApplyConfiguration(new FollowConfiguration());
        }
    }
}